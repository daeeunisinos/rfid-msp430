################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../NFC/felica.c \
../NFC/iso14443a.c \
../NFC/iso14443b.c \
../NFC/iso15693.c \
../NFC/ndef.c \
../NFC/nfc_app.c 

C_DEPS += \
./NFC/felica.d \
./NFC/iso14443a.d \
./NFC/iso14443b.d \
./NFC/iso15693.d \
./NFC/ndef.d \
./NFC/nfc_app.d 

OBJS += \
./NFC/felica.obj \
./NFC/iso14443a.obj \
./NFC/iso14443b.obj \
./NFC/iso15693.obj \
./NFC/ndef.obj \
./NFC/nfc_app.obj 

OBJS__QUOTED += \
"NFC\felica.obj" \
"NFC\iso14443a.obj" \
"NFC\iso14443b.obj" \
"NFC\iso15693.obj" \
"NFC\ndef.obj" \
"NFC\nfc_app.obj" 

C_DEPS__QUOTED += \
"NFC\felica.d" \
"NFC\iso14443a.d" \
"NFC\iso14443b.d" \
"NFC\iso15693.d" \
"NFC\ndef.d" \
"NFC\nfc_app.d" 

C_SRCS__QUOTED += \
"../NFC/felica.c" \
"../NFC/iso14443a.c" \
"../NFC/iso14443b.c" \
"../NFC/iso15693.c" \
"../NFC/ndef.c" \
"../NFC/nfc_app.c" 


