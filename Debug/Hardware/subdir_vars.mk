################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Hardware/VLO_Library.asm 

C_SRCS += \
../Hardware/mcu.c \
../Hardware/spi.c \
../Hardware/trf79xxa.c \
../Hardware/uart.c 

C_DEPS += \
./Hardware/mcu.d \
./Hardware/spi.d \
./Hardware/trf79xxa.d \
./Hardware/uart.d 

OBJS += \
./Hardware/VLO_Library.obj \
./Hardware/mcu.obj \
./Hardware/spi.obj \
./Hardware/trf79xxa.obj \
./Hardware/uart.obj 

ASM_DEPS += \
./Hardware/VLO_Library.d 

OBJS__QUOTED += \
"Hardware\VLO_Library.obj" \
"Hardware\mcu.obj" \
"Hardware\spi.obj" \
"Hardware\trf79xxa.obj" \
"Hardware\uart.obj" 

C_DEPS__QUOTED += \
"Hardware\mcu.d" \
"Hardware\spi.d" \
"Hardware\trf79xxa.d" \
"Hardware\uart.d" 

ASM_DEPS__QUOTED += \
"Hardware\VLO_Library.d" 

ASM_SRCS__QUOTED += \
"../Hardware/VLO_Library.asm" 

C_SRCS__QUOTED += \
"../Hardware/mcu.c" \
"../Hardware/spi.c" \
"../Hardware/trf79xxa.c" \
"../Hardware/uart.c" 


